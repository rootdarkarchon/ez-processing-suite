// fill in name
#feature-id EZ Processing Suite > EZ //NAME

// plugin name
var NAME = '';
// plugin version
var VERSION = '';
// author of plugin
var AUTHOR = "";

// always include after feature-id, name, version and author
#include "EZ_Common.js"

function saveSettings() {
    // save all settings here
}

function generateProcessingInfo() {
    // should return a new ProcessingInfo derivative object
    // will be saved in CurrentProcessingInfo
}

function execute(window, bringToFront = true, runOnMain = false) {
    // is called on dialog.ok result
}

function onExit() { 
    // is called on exiting module
}

function onInit() { 
    // is called on initializing module
}

function customizeDialog() {
    // IMPORTANT:
    // do NOT use sizer.add() or sizer.insert() or sizer.remove()
    // use sizer.addItem(), sizer.insertItem() and sizer.removeItem() instead
    // otherwise bindings of inherited controls will not be called.
    // to use bindings on controls, add control.bindings = function() { doWhatever } 
    // ideally bindings reflect the same value as used onValueUpdated or whatever
    // so it can be set during loading or during reset automatically
    // bindings will be called periodically, it is possible to i.e. set button state and other things at the same time
    // for long processes call startProcess() and endProcess()
    // startProcess() will increase PROCESSING stack
    // endProcess() will decrease PROCESSING stack
    // dialog.enabled is bound to PROCESSING.length > 0

    // customize dialog appearance and everything
    dialog.infoBox.text = "This is the info text displayed on top";
    dialog.tutorialPrerequisites = ["One entry is one prerequisite"];
    dialog.tutorialSteps = ["One line is one step"]

    // custom binding for the dialog only, use .bindings for everything else!
    dialog.customBindings = function() { }

    // uncomment if evaluate button should be shown
    // dialog.onEvaluateButton.show()

    dialog.onExit = function() { 
        // called on exiting the dialog
    }

    // uncomment if previews should be allowed to be chosen
    // dialog.allowPreviews();

    dialog.onSelectedMainView = function(view) {
        // called on selecting the main view
    }

    dialog.onEmptyMainView = function(view) {
        // called when main view is selected but empty
    }

    dialog.canRun = function() {
        // called for checking whether main run button can be run
        // is bound to dialog.runEverythingButton.enabled which will close the dialog and this will run execute()
        return true;
    }

    dialog.canEvaluate = function() {
        // called for checking whether evaluate button can be run
        // is bound to dialog.onEvaluateButton.enabled which will call onEvaluate
        return true;
    }

    dialog.onEvaluate = function() {
        // called when evaluate button is pressed
        // prior PROCESSING stack will increase and after decrease again
    }

    // this is to replace the basic main control for more functionality, it is by default a groupbox
    // dialog.controlSizer.insertItem(3, YOURNEWCONTROL);
	// dialog.controlSizer.removeItem(dialog.mainControl);
	// dialog.mainControl.hide();
}

main();